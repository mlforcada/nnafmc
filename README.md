= Neural Networks, Automata and Formal Models of Computation =

In 2002 I announced the availability of a web document, "Neural Networks, Automata, and Formal Models of Computation". The URL is  http://www.dlsi.ua.es/~mlf/nnafmc/ ; you can find there  both a printable PDF file and a browsable (imperfect) HTML version. 

"Neural Networks, Automata, and Formal Models of Computation" was initially conceived (in 1995) as a reprint collection.  A number of
personal and editorial circumstances have prevented me from finishing this work; therefore, this document can only be seen as some kind of draft, besides being somewhat outdated (reflecting perhaps the state of things around 1999). But I am making it publically available, just in case it is useful to other people working in related fields. By that time I was switching to a different field of computer science, and I thought that perhaps it was better to have it on the web than buried in my hard disk. 

Now, with the revival of neural approaches to human language processing, some people have shown an interest in this work. I am therefore uploading the sources under the GPL v3 license.

The work of Juan Antonio Pérez Ortiz (mainly when retyping Kleene's 1956 paper) is gladly acknowledged.
