{ t=$0;
while (match(t,/\\[a-z]+cite(\[[^\]]+\])?{[^}]+}/))
{
  p=substr(t,RSTART,RLENGTH);
  citations[p]++
  t=substr(t,RSTART+RLENGTH);
}
}
END { 
printf "{"
{for (p in citations)  { 
    match(p,/{[^}]+}/);
    key=substr(p,RSTART+1,RLENGTH-2);
    match(p,/\\[a-z]+cite/ )
    command=substr(p,RSTART+1,RLENGTH-1);
    if match(p,/\[[^\]]+\]/)
    pages=substr(p,RSTART+1,RLENGTH-1);
    printf("gsub(/\\\\" command 
}
}
}

