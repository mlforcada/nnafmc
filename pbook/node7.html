<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">

<!--Converted with LaTeX2HTML 99.2beta6 (1.42)
original version by:  Nikos Drakos, CBLU, University of Leeds
* revised and updated by:  Marcus Hennecke, Ross Moore, Herb Swan
* with significant contributions from:
  Jens Lippmann, Marek Rouchal, Martin Wilck and others -->
<HTML>
<HEAD>
<TITLE>Neural networks and formal models of language and computation</TITLE>
<META NAME="description" CONTENT="Neural networks and formal models of language and computation">
<META NAME="keywords" CONTENT="pbook">
<META NAME="resource-type" CONTENT="document">
<META NAME="distribution" CONTENT="global">

<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<META NAME="Generator" CONTENT="LaTeX2HTML v99.2beta6">
<META HTTP-EQUIV="Content-Style-Type" CONTENT="text/css">

<LINK REL="STYLESHEET" HREF="pbook.css">

<LINK REL="next" HREF="node8.html">
<LINK REL="previous" HREF="node6.html">
<LINK REL="up" HREF="node6.html">
<LINK REL="next" HREF="node8.html">
</HEAD>

<BODY >
<!--Navigation Panel-->
<A NAME="tex2html2049"
  HREF="node8.html">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next" SRC="next.png"></A> 
<A NAME="tex2html2043"
  HREF="node6.html">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up" SRC="up.png"></A> 
<A NAME="tex2html2037"
  HREF="node6.html">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous" SRC="prev.png"></A> 
<A NAME="tex2html2045"
  HREF="node1.html">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents" SRC="contents.png"></A> 
<A NAME="tex2html2047"
  HREF="node93.html">
<IMG WIDTH="43" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="index" SRC="index.png"></A> 
<BR>
<B> Next:</B> <A NAME="tex2html2050"
  HREF="node8.html">Organization of the document</A>
<B> Up:</B> <A NAME="tex2html2044"
  HREF="node6.html">Introduction</A>
<B> Previous:</B> <A NAME="tex2html2038"
  HREF="node6.html">Introduction</A>
 &nbsp <B>  <A NAME="tex2html2046"
  HREF="node1.html">Contents</A></B> 
 &nbsp <B>  <A NAME="tex2html2048"
  HREF="node93.html">Index</A></B> 
<BR>
<BR>
<!--End of Navigation Panel-->

<H1><A NAME="SECTION00310000000000000000">
Neural networks and formal models of language and computation</A>
</H1>

<P>
The last decade has witnessed a surge of interest in the relationships
between the behavior of artificial neural networks and models used in
formal language theory and theory of computation, such as automata or
Turing machines. As a result, an increasing number of researchers are
focusing their attention in questions such as

<UL>
<LI>``what can a neural network compute?'', and --since neural
  networks are by nature trainable systems--
</LI>
<LI>``what can a neural network <EM>learn</EM> to compute?''
</LI>
</UL>
Distinguishing these two apparently equivalent questions is crucial in
the field of artificial neural networks. It is often the case that
neural network architectures that may be shown to be capable of a
certain kind of computation cannot easily be trained from examples to
perform it. Unfortunately, one may found examples in the literature
where researchers attribute an observed problem to the architecture used
when it may also be due to the training algorithm (learning algorithm) used.

<P>
These questions --which will be called the <EM>main questions</EM> in this
Introduction-- tie together the cores of two broad fields of basic
knowledge: computer science and neuroscience.
The convergence of their daughter
disciplines (such as pattern recognition, artificial intelligence,
neurocognition, etc.)
in the interdisciplinary arena of artificial neural networks may be
reasonably expected to have a great technological impact and a wealth of
applications.

<P>
Indeed, on the one hand, in computer science, the formal theories of
language and computation are so intimately related that they may be
considered to form a single body of knowledge. One may just take a
look at the titles of highly cited and well known books on the
subject; as an example, take the <EM>Introduction to
  automata theory, languages and computation</EM> by <A
 HREF="node95.html#Hop79a">Hopcroft and Ullman (1979)</A>. There is a
well-established relationship between the levels in Chomsky's
hierarchy of language complexity
(regular, context-free, context-sensitive and unrestricted, in order
of increasing complexity) and the classes of idealized machines,
(correspondingly, finite-state automata, pushdown or stack automata,
linearly bounded automata and Turing machines, in increasing order of
computational power). This equivalence allows us to view computational
devices --that is, automata-- as language acceptors or language
recognizers and
their computational power in terms of the complexity of the language
class they accept or recognize.  

<P>
On the other hand, the advances in neuroscience have inspired
idealized models of the brain and the nervous system. Extremely
simplified models of the behavior of an isolated neuron and of the
interaction between neurons have been used to construct the concept of
<EM>artificial neural networks</EM>, systems that are capable of acting
as universal function approximators (<A
 HREF="node95.html#Hor89a">Hornik et&nbsp;al., 1989</A>), amenable to be
trained from examples without the need for a thorough understanding of
the task in hand, and able to show surprising generalization
performance and predicting power, thus mimicking some interesting
cognitive properties of evolved natural neural systems such as the
brains of mammals.

<P>
In the light of the equivalence between language and automata classes,
and the wide variety of neural architectures --more specifically,
discrete-time recurrent neural networks-- that can be used to emulate
the behavior of sequential machines, the
basic questions stated at the beginning of this introduction may be
given a more concrete formulation: on the one hand,

<UL>
<LI>can a neural network of architecture class <IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img7.png"
 ALT="$A$"> perform the same
computation as an automaton of class <IMG
 WIDTH="21" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img8.png"
 ALT="$M$">?
</LI>
<LI>can a neural network of architecture class <IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img7.png"
 ALT="$A$"> be a
  recognizer for
languages of language class <IMG
 WIDTH="15" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img9.png"
 ALT="$L$">?
</LI>
</UL>
and on the other hand,

<UL>
<LI>can a neural network of architecture class <IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img7.png"
 ALT="$A$"> <EM>be trained
<A NAME="tex2html5"
  HREF="footnode.html#foot3317"><SUP>2.1</SUP></A> to</EM> perform the same computation as an automaton of class <IMG
 WIDTH="21" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img8.png"
 ALT="$M$">,
from a set of examples?
</LI>
<LI>can a neural network of architecture class <IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img7.png"
 ALT="$A$"> <EM>be trained
to</EM> recognize a language of class <IMG
 WIDTH="15" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img9.png"
 ALT="$L$"> from a set of examples?
</LI>
</UL>
Viewing computing devices as language recognizers or language
acceptors 
allows researchers to view examples as strings, belonging or not to
the language --to the computation-- that is to be learned by the
neural net. This brings the research in this field very close to
another important area of computer science: <EM>grammatical
  inference</EM>, that is learning grammars from examples. Grammatical
inference is relevant because a wide variety of phenomena may be
represented and treated as languages after
discretization of the input
sequences as strings of symbols from an appropriate
alphabet.

<P>
One of the first contacts of these two wide streams of research may be
traced to a date as early as 1943, when two researchers from MIT,
Warren S. McCulloch and Walter Pitts, published their paper ``A
logical calculus of the ideas immanent in nervous activity''
(<A
 HREF="node95.html#McC43a">McCulloch and Pitts, 1943</A>)<A NAME="73"></A><A NAME="74"></A>. This paper  is considered to be seminal to both the
field of artificial neural networks and to that of automata theory. As
<A
 HREF="node95.html#perrin90b">Perrin (1990)</A><A NAME="76"></A> put it, McCulloch and Pitts presented ``a logical
model for the behaviour of nervous systems that turned out to be the
model of a finite-state machine''. This contact was followed a decade
later by the work of <A
 HREF="node95.html#kleene56b">Kleene (1956)</A><A NAME="78"></A>
and <A
 HREF="node95.html#minsky56b">Minsky (1956)</A><A NAME="80"></A>,
who gradually moved away from the neural formalization toward a
logical and mathematical layout that would be called ``finite
automata''. It was <A
 HREF="node95.html#minsky67b">Minsky (1967)</A><A NAME="82"></A>
 who made the often-quoted
statement ``every finite-state machine is equivalent to, and can be
simulated by, some neural net''.

<P>
A long hiatus of more than three decades followed that early junction.
The field of artificial neural networks grew falteringly in the
beginning and then bloomed exuberantly in the late seventies and the
early eighties with the work of <A
 HREF="node95.html#Wer74a">Werbos (1974)</A><A NAME="84"></A>,
<A
 HREF="node95.html#Kohonen74">Kohonen (1974)</A><A NAME="86"></A>,
<A
 HREF="node95.html#hopfield">Hopfield (1982)</A><A NAME="88"></A>, and
<A
 HREF="node95.html#McClelland86a">McClelland et&nbsp;al. (1986)</A><A NAME="90"></A><A NAME="91"></A>.  The formal theory of language and
computation has been maturing steadily during this time.  It was not
until the late eighties that both fields made contact again, this time
for a long and fertile relationship. This document is a selection of
what I believe to be some of the best representatives of the work
that has been done in the field that connects artificial neural
networks, automata and formal models of language and computation.

<P>
<HR>
<!--Navigation Panel-->
<A NAME="tex2html2049"
  HREF="node8.html">
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next" SRC="next.png"></A> 
<A NAME="tex2html2043"
  HREF="node6.html">
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up" SRC="up.png"></A> 
<A NAME="tex2html2037"
  HREF="node6.html">
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous" SRC="prev.png"></A> 
<A NAME="tex2html2045"
  HREF="node1.html">
<IMG WIDTH="65" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="contents" SRC="contents.png"></A> 
<A NAME="tex2html2047"
  HREF="node93.html">
<IMG WIDTH="43" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="index" SRC="index.png"></A> 
<BR>
<B> Next:</B> <A NAME="tex2html2050"
  HREF="node8.html">Organization of the document</A>
<B> Up:</B> <A NAME="tex2html2044"
  HREF="node6.html">Introduction</A>
<B> Previous:</B> <A NAME="tex2html2038"
  HREF="node6.html">Introduction</A>
 &nbsp <B>  <A NAME="tex2html2046"
  HREF="node1.html">Contents</A></B> 
 &nbsp <B>  <A NAME="tex2html2048"
  HREF="node93.html">Index</A></B> 
<!--End of Navigation Panel-->
<ADDRESS>
Debian User
2002-01-21
</ADDRESS>
</BODY>
</HTML>
