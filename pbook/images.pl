# LaTeX2HTML 99.2beta6 (1.42)
# Associate images original text with physical files.


$key = q/gammainGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$\gamma \in \Gamma $">|; 

$key = q/{displaymath}L(G)={{{tt{tomato},{{tt{tomamato},{{tt{tomamamato}ldots}.{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="313" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img276.png"
 ALT="\begin{displaymath}L(G)=\{ {\tt tomato}, {\tt tomamato}, {\tt tomamamato} \ldots \}.\end{displaymath}">|; 

$key = q/n_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img291.png"
 ALT="$n_0$">|; 

$key = q/{displaymath}h_i({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_X}W_{ij}^{yx}x_j[t-1]+sum_{j=1}^{n_U}W_{ij}^{yu}u_j[t]+W^y_iright).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="445" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img167.png"
 ALT="\begin{displaymath}
h_i({\bf x}[t-1],{\bf u}[t])=g\left(\sum_{j=1}^{n_X} W_{ij}^...
...j[t-1] +
\sum_{j=1}^{n_U} W_{ij}^{yu} u_j[t] +
W^y_i\right).
\end{displaymath}">|; 

$key = q/sigma_kinSigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img321.png"
 ALT="$\sigma_k \in\Sigma$">|; 

$key = q/u[1]u[2]ldots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="77" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img388.png"
 ALT="$u[1]u[2]\ldots$">|; 

$key = q/x[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img111.png"
 ALT="$x[t]$">|; 

$key = q/{{bf{c}[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img399.png"
 ALT="${\bf c}[t]$">|; 

$key = q/xxu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img155.png"
 ALT="$xxu$">|; 

$key = q/{displaymath}x[t]=f(x[t-1],u[t]).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="153" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img116.png"
 ALT="\begin{displaymath}
x[t]=f(x[t-1],u[t]).
\end{displaymath}">|; 

$key = q/|Q|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img286.png"
 ALT="$\vert Q\vert$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$i$">|; 

$key = q/{n_X}={n_U}n_I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="84" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img203.png"
 ALT="${n_X} = {n_U}n_I$">|; 

$key = q/y[1]y[2]ldots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="76" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img110.png"
 ALT="$y[1]y[2]\ldots$">|; 

$key = q/q^{n_X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img393.png"
 ALT="$q^{n_X}$">|; 

$key = q/2^{n_X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img301.png"
 ALT="$2^{n_X}$">|; 

$key = q/q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img394.png"
 ALT="$q$">|; 

$key = q/includegraphics{narx};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="455" HEIGHT="392" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img201.png"
 ALT="\includegraphics{narx}">|; 

$key = q/Y_{calY}capY_{calN}=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="93" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img349.png"
 ALT="$Y_{\cal Y}\cap Y_{\cal N}=\emptyset$">|; 

$key = q/alphainV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img280.png"
 ALT="$\alpha\in V$">|; 

$key = q/n_H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img234.png"
 ALT="$n_H$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img103.png"
 ALT="$u$">|; 

$key = q/y[t]=x_1[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img179.png"
 ALT="$y[t]=x_1[t]$">|; 

$key = q/{{bf{u}[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img135.png"
 ALT="${\bf u}[t]$">|; 

$key = q/{{bf{x}[t-1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img134.png"
 ALT="${\bf x}[t-1]$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$y$">|; 

$key = q/delta(q_2,0)=q_3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img76.png"
 ALT="$\delta(q_2,0)=q_3$">|; 

$key = q/{{bf{x}[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img133.png"
 ALT="${\bf x}[t]$">|; 

$key = q/s^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img89.png"
 ALT="$s^*$">|; 

$key = q/{{bf{y}[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img139.png"
 ALT="${\bf y}[t]$">|; 

$key = q/n=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="$n=1$">|; 

$key = q/{{bf{W}^{yx});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img300.png"
 ALT="${\bf W}^{yx})$">|; 

$key = q/theta_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="$\theta_i$">|; 

$key = q/L_u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img101.png"
 ALT="$L_u$">|; 

$key = q/Theta((|Q|log|Q|)^{1slash2});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="130" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img310.png"
 ALT="$\Theta((\vert Q\vert\log \vert Q\vert)^{1/2})$">|; 

$key = q/delta:QtimesSigmarightarrowQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="106" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img39.png"
 ALT="$\delta:Q\times \Sigma\rightarrow Q$">|; 

$key = q/S_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img126.png"
 ALT="$S_1$">|; 

$key = q/L_y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img102.png"
 ALT="$L_y$">|; 

$key = q/n_X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img127.png"
 ALT="$n_X$">|; 

$key = q/O(F(n));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img288.png"
 ALT="$O(F(n))$">|; 

$key = q/lambda(q_2,1)={{tt{E};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img58.png"
 ALT="$\lambda(q_2,1)={\tt E}$">|; 

$key = q/[S_0,S_1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img250.png"
 ALT="$[S_0,S_1]$">|; 

$key = q/omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img360.png"
 ALT="$\omega$">|; 

$key = q/x_i[t-1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="61" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img228.png"
 ALT="$x_i[t-1]$">|; 

$key = q/{displaymath}h_i({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_Z}W_{ij}^{xz}z_j({{bf{x}[t-1],{{bf{u}[t])+W_i^xright).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img173.png"
 ALT="\begin{displaymath}
h_i({\bf x}[t-1],{\bf u}[t]) = g\left( \sum_{j=1}^{n_Z} W_{ij}^{xz}
z_j({\bf x}[t-1],{\bf u}[t])
+ W_i^x\right).
\end{displaymath}">|; 

$key = q/{displaymath}h_i({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_X}sum_{k=1}^{n_U}W^{yxu}_{ijk}x_j[t-1]u_k[t]+W^y_iright).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="401" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img154.png"
 ALT="\begin{displaymath}
h_i({\bf x}[t-1],{\bf u}[t])=
g\left( \sum_{j=1}^{n_X} \sum_{k=1}^{n_U} W^{yxu}_{ijk} x_j[t-1] u_k[t] +
W^y_i \right).
\end{displaymath}">|; 

$key = q/y_i[t]=x_i[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="82" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img176.png"
 ALT="$y_i[t]=x_i[t]$">|; 

$key = q/L_1L_2={w_1w_2|w_1inL_1,;w_2inL_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="242" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img269.png"
 ALT="$L_1L_2=\{w_1w_2\vert w_1\in
L_1,\; w_2\in L_2\}$">|; 

$key = q/delta(q_1,0)=q_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img74.png"
 ALT="$\delta(q_1,0)=q_2$">|; 

$key = q/x_i[t]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$x_i[t]=0$">|; 

$key = q/includegraphics[scale=0.8]{fig018};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="384" HEIGHT="116" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img82.png"
 ALT="\includegraphics[scale=0.8]{fig018}">|; 

$key = q/beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img265.png"
 ALT="$\beta$">|; 

$key = q/i=1,ldots,|Q|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="95" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img341.png"
 ALT="$i=1,\ldots,\vert Q\vert$">|; 

$key = q/11ldots10000ldots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img376.png"
 ALT="$11\ldots 10000\ldots$">|; 

$key = q/g_sigma(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img385.png"
 ALT="$g_\sigma(x)$">|; 

$key = q/Theta(F(n));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img298.png"
 ALT="$\Theta(F(n))$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img34.png"
 ALT="$0$">|; 

$key = q/XrightarrowYZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img401.png"
 ALT="$X\rightarrow YZ$">|; 

$key = q/4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img358.png"
 ALT="$4$">|; 

$key = q/m=1,ldots,|Gamma|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img342.png"
 ALT="$m=1,\ldots,\vert\Gamma\vert$">|; 

$key = q/{displaymath}{{bf{h}_k(X_j)subseteqY_m;;;forallq_jinQ,sigma_kinSigma:lambda(q_j,sigma_k)=gamma_m{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="329" HEIGHT="29" BORDER="0"
 SRC="|."$dir".q|img337.png"
 ALT="\begin{displaymath}
{\bf h}_k(X_j)\subseteq Y_m \;\;\;\forall q_j\in Q,\sigma_k\in\Sigma :
\lambda(q_j,\sigma_k)=\gamma_m
\end{displaymath}">|; 

$key = q/delta(q_1,1)=q_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img52.png"
 ALT="$\delta(q_1,1)=q_2$">|; 

$key = q/Sigma^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img67.png"
 ALT="$\Sigma^*$">|; 

$key = q/i=1,ldots,n_X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img147.png"
 ALT="$i=1,\ldots,n_X$">|; 

$key = q/delta(q_4,0)=q_4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img79.png"
 ALT="$\delta(q_4,0)=q_4$">|; 

$key = q/{{bf{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img137.png"
 ALT="${\bf h}$">|; 

$key = q/2^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="$2^n$">|; 

$key = q/M=(Q,Sigma,Phi,delta,q_I,phi_0,F);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img281.png"
 ALT="$M=(Q,\Sigma,\Phi,\delta,q_I,\phi_0,F)$">|; 

$key = q/@;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img381.png"
 ALT="$@$">|; 

$key = q/u[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img115.png"
 ALT="$u[t]$">|; 

$key = q/D;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img259.png"
 ALT="$D$">|; 

$key = q/L(G);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img271.png"
 ALT="$L(G)$">|; 

$key = q/f(n)leKF(n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="106" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img294.png"
 ALT="$f(n)\le KF(n)$">|; 

$key = q/H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img355.png"
 ALT="$H$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$L$">|; 

$key = q/{displaymath}h_i({{bf{x}[t])=gleft(sum_{j=1}^{n_X}W_{ij}^{yx}x_j[t]+W^y_iright).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="250" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img175.png"
 ALT="\begin{displaymath}
h_i({\bf x}[t])=g\left(\sum_{j=1}^{n_X} W_{ij}^{yx} x_j[t] +
W^y_i\right).
\end{displaymath}">|; 

$key = q/X_icapX_j=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img319.png"
 ALT="$X_i\cap X_j=\emptyset$">|; 

$key = q/x_j[t-1],jneqi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="103" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img229.png"
 ALT="$x_j[t-1], j\neq
i$">|; 

$key = q/W_{ij}^{xx'};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img236.png"
 ALT="$W_{ij}^{xx'}$">|; 

$key = q/P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img261.png"
 ALT="$P$">|; 

$key = q/Theta(|Q|);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img311.png"
 ALT="$\Theta(\vert Q\vert)$">|; 

$key = q/{{bf{x}[t]inX_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img318.png"
 ALT="${\bf x}[t]\in X_i$">|; 

$key = q/{{bf{f}:X^{V}timesUrightarrowX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="121" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img245.png"
 ALT="${\bf f}:X^{V}\times
U\rightarrow X$">|; 

$key = q/T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img97.png"
 ALT="$T$">|; 

$key = q/partialx_i[0]slashpartialx_j[0]=delta_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="134" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img220.png"
 ALT="$\partial
x_i[0]/\partial x_j[0]=\delta_{ij}$">|; 

$key = q/Sigma={1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="61" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img375.png"
 ALT="$\Sigma=\{1\}$">|; 

$key = q/delta(q_j,sigma_k)=q_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img396.png"
 ALT="$\delta(q_j,\sigma_k)=q_n$">|; 

$key = q/{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img90.png"
 ALT="$\{\}$">|; 

$key = q/U={mathbb{R}{^{n_U};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img128.png"
 ALT="$U=\mathbb{R}^{n_U}$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img251.png"
 ALT="$X$">|; 

$key = q/{displaymath}{{bf{f}_k(A)={{{bf{f}({{bf{x},{{bf{u}_k):{{bf{x}inA}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img334.png"
 ALT="\begin{displaymath}
{\bf f}_k(A)=\{{\bf f}({\bf x}, {\bf u}_k) : {\bf x}\in A \}
\end{displaymath}">|; 

$key = q/q_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img40.png"
 ALT="$q_j$">|; 

$key = q/{displaymath}f_i({{bf{x}[t-1],{{bf{u}[t])=alphax_i[t-1]+gleft(sum_{j=1}^{n_Z}W_{ij}^{xz}z_j({{bf{x}[t-1],{{bf{u}[t])+W_i^xright){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="463" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img171.png"
 ALT="\begin{displaymath}
f_i({\bf x}[t-1],{\bf u}[t]) = \alpha x_i[t-1]
+ g\left( \s...
...{n_Z} W_{ij}^{xz} z_j({\bf x}[t-1],{\bf u}[t])
+ W_i^x\right)
\end{displaymath}">|; 

$key = q/{displaymath}{{bf{h}_k(A)={{{bf{h}({{bf{x},{{bf{u}_k):{{bf{x}inA}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="190" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img338.png"
 ALT="\begin{displaymath}
{\bf h}_k(A)=\{{\bf h}({\bf x},{\bf u}_k) : {\bf x}\in A \}
\end{displaymath}">|; 

$key = q/delta(q_4,1)=q_4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img80.png"
 ALT="$\delta(q_4,1)=q_4$">|; 

$key = q/q_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img395.png"
 ALT="$q_n$">|; 

$key = q/Gamma={gamma_1,gamma_2,ldots,gamma_{|Gamma|}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="149" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img38.png"
 ALT="$\Gamma=\{\gamma_1,\gamma_2,\ldots,\gamma_{\vert\Gamma\vert}\}$">|; 

$key = q/{displaymath}{{bf{f}_k(X_j)subseteqX_i;;;forallq_jinQ,sigma_kinSigma:delta(q_j,sigma_k)=q_i{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="312" HEIGHT="29" BORDER="0"
 SRC="|."$dir".q|img333.png"
 ALT="\begin{displaymath}
{\bf f}_k(X_j)\subseteq X_i \;\;\;\forall q_j\in Q,\sigma_k\in\Sigma : \delta(q_j,\sigma_k)=q_i
\end{displaymath}">|; 

$key = q/x[t-1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img114.png"
 ALT="$x[t-1]$">|; 

$key = q/Omega(|Q|);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img306.png"
 ALT="$\Omega(\vert Q\vert)$">|; 

$key = q/|alpha|le|beta|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img278.png"
 ALT="$\vert\alpha\vert\le\vert\beta\vert$">|; 

$key = q/x<0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img386.png"
 ALT="$x&lt;0$">|; 

$key = q/W_{ijk}^{xxu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img156.png"
 ALT="$W_{ijk}^{xxu}$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img121.png"
 ALT="$h$">|; 

$key = q/delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img64.png"
 ALT="$\delta$">|; 

$key = q/QtimesSigma^*timesPhi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img284.png"
 ALT="$Q\times\Sigma^*\times\Phi$">|; 

$key = q/{{bf{u}[1]ldots{{bf{u}[t-1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="107" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img239.png"
 ALT="${\bf u}[1]\ldots {\bf u}[t-1]$">|; 

$key = q/sigmaslashgamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$\sigma /\gamma $">|; 

$key = q/hat{u}[t+1]hat{u}[t+2]...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="120" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img109.png"
 ALT="$\hat{u}[t+1]\hat{u}[t+2]...$">|; 

$key = q/n_C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img231.png"
 ALT="$n_C$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img208.png"
 ALT="$p$">|; 

$key = q/i=k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img217.png"
 ALT="$i=k$">|; 

$key = q/t-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$t-1$">|; 

$key = q/{displaymath}G=(V,Sigma,P,S){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="109" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img272.png"
 ALT="\begin{displaymath}G = (V,\Sigma,P,S)\end{displaymath}">|; 

$key = q/n_X=|Q||Sigma|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img312.png"
 ALT="$n_X=\vert Q\vert\vert\Sigma\vert$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$t$">|; 

$key = q/f:mathbb{N}rightarrowmathbb{R};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img290.png"
 ALT="$f:\mathbb{N}\rightarrow\mathbb{R}$">|; 

$key = q/{{bf{d}_i:XrightarrowX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img248.png"
 ALT="${\bf d}_i: X \rightarrow X$">|; 

$key = q/q_I=q_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img48.png"
 ALT="$q_I=q_1$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img157.png"
 ALT="$x$">|; 

$key = q/delta(q_2,0)=q_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img53.png"
 ALT="$\delta(q_2,0)=q_2$">|; 

$key = q/X=[S_0,S_1]^{n_X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img124.png"
 ALT="$X=[S_0,S_1]^{n_X}$">|; 

$key = q/N_i(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$N_i(t)$">|; 

$key = q/n_O;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img181.png"
 ALT="$n_O$">|; 

$key = q/{displaymath}y_i[t]=gleft(sum_{j=1}^{n_U}W_{ij}^{yu}u_j[t]+W^y_iright);,i=1ldotsn_Y,{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="317" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img230.png"
 ALT="\begin{displaymath}
y_i[t]=g\left(\sum_{j=1}^{n_U} W_{ij}^{yu} u_j[t] + W^y_i \right)\;,
i=1\ldots n_Y,
\end{displaymath}">|; 

$key = q/{displaymath}{{bf{y}[t]={{bf{h}({{bf{x}[t-1],{{bf{u}[t]),{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="155" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img140.png"
 ALT="\begin{displaymath}
{\bf y}[t]={\bf h}({\bf x}[t-1],{\bf u}[t]),
\end{displaymath}">|; 

$key = q/Sigma={{{tt{0},{{tt{1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="76" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img49.png"
 ALT="$\Sigma=\{{\tt0}, {\tt 1}\}$">|; 

$key = q/g:{mathbb{R}{rightarrow[S_0,S_1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="112" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img149.png"
 ALT="$g:\mathbb{R}\rightarrow [S_0,S_1]$">|; 

$key = q/X_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img330.png"
 ALT="$X_j$">|; 

$key = q/{displaymath}x_i[t]=g_sigmaleft(sum_{j=1}^{n_X}W^{xx}_{ij}x_j[t-1]+W^{xu}_{i}u[t]+W_i^xright){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="330" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img383.png"
 ALT="\begin{displaymath}x_i[t]=g_\sigma\left(\sum_{j=1}^{n_X} W^{xx}_{ij} x_j[t-1] +
W^{xu}_{i} u[t] + W_i^x\right)\end{displaymath}">|; 

$key = q/S_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img125.png"
 ALT="$S_0$">|; 

$key = q/u=u[1]u[2]ldotsu[L_u];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="147" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img98.png"
 ALT="$u=u[1]u[2]\ldots u[L_u]$">|; 

$key = q/alphain[0,1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img172.png"
 ALT="$\alpha\in[0,1]$">|; 

$key = q/n_On_Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img187.png"
 ALT="$n_On_Y$">|; 

$key = q/gamma_mneqgamma_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img328.png"
 ALT="$\gamma_m\neq\gamma_n$">|; 

$key = q/n_X>n_U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img212.png"
 ALT="$n_X&gt;n_U$">|; 

$key = q/{displaymath}h_i({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_Z}W^{yz}_{ij}z_j[t]+W^y_iright){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="296" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img352.png"
 ALT="\begin{displaymath}
h_i({\bf x}[t-1],{\bf u}[t])=g\left(\sum_{j=1}^{n_Z} W^{yz}_{ij} z_j[t]
+ W^y_i \right)
\end{displaymath}">|; 

$key = q/lambda(q_1,0)={{tt{E};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img55.png"
 ALT="$\lambda(q_1,0)={\tt E}$">|; 

$key = q/q_s=0.1111=15slash16;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="145" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img389.png"
 ALT="$q_s=0.1111=15/16$">|; 

$key = q/{displaymath}z_i[t]=gleft(sum_{j=1}^{n_X}W_{ij}^{zx}x_j[t-1]+sum_{j=1}^{n_U}W_{ij}^{zu}u_j[t]+W^z_iright){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="350" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img353.png"
 ALT="\begin{displaymath}
z_i[t] =g\left(\sum_{j=1}^{n_X} W_{ij}^{zx} x_j[t-1] +
\sum_{j=1}^{n_U} W_{ij}^{zu} u_j[t] +
W^z_i\right)
\end{displaymath}">|; 

$key = q/pbmodomega=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="91" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img361.png"
 ALT="$p\bmod\omega=0$">|; 

$key = q/phi_I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img283.png"
 ALT="$\phi_I$">|; 

$key = q/b_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$b_i$">|; 

$key = q/{displaymath}{{bf{x}[t]={{bf{f}({{bf{x}[t-1],{{bf{u}[t]);{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="153" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img136.png"
 ALT="\begin{displaymath}
{\bf x}[t]={\bf f}({\bf x}[t-1],{\bf u}[t]);
\end{displaymath}">|; 

$key = q/{{bf{W};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img223.png"
 ALT="${\bf W}$">|; 

$key = q/S_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img150.png"
 ALT="$S_0=0$">|; 

$key = q/q_i=delta(q_j,sigma_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img332.png"
 ALT="$q_i=\delta(q_j,\sigma_k)$">|; 

$key = q/{displaymath}array{{rlc}f_i({{bf{x}[t-1],{{bf{u}[t])=&x_{i-{n_U}}[t-1],&{n_U}<il_i({{bf{x}[t-1],{{bf{u}[t])=&u_i[t],&1leile{n_U},array{{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="355" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img202.png"
 ALT="\begin{displaymath}
\begin{array}{rlc} f_i({\bf x}[t-1],{\bf u}[t]) =&amp; x_{i-{n_U...
...f x}[t-1],{\bf u}[t]) =&amp; u_i[t],&amp;
1\le i\le {n_U},
\end{array}\end{displaymath}">|; 

$key = q/Omega((|Q|log|Q|)^{1slash3});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="129" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img304.png"
 ALT="$\Omega((\vert Q\vert\log \vert Q\vert)^{1/3})$">|; 

$key = q/delta_{ik};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img216.png"
 ALT="$\delta_{ik}$">|; 

$key = q/delta(q_1,0)=q_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img51.png"
 ALT="$\delta(q_1,0)=q_1$">|; 

$key = q/n_X^4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img221.png"
 ALT="$n_X^4$">|; 

$key = q/BinLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img368.png"
 ALT="$B\in \Lambda$">|; 

$key = q/u[1],ldots,u[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img112.png"
 ALT="$u[1], \ldots, u[t]$">|; 

$key = q/g_T(x)={{rm{tanh}(x)=(1-exp(-2x))slash(1+exp(-2x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="359" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img163.png"
 ALT="$g_T(x)={\rm tanh}(x)=(1-\exp(-2x))/(1+\exp(-2x))$">|; 

$key = q/Omega(F(n));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img295.png"
 ALT="$\Omega(F(n))$">|; 

$key = q/M=(Q,Sigma,Gamma,delta,lambda,q_I);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="155" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img46.png"
 ALT="$M=(Q,\Sigma,\Gamma,\delta,\lambda,q_I)$">|; 

$key = q/1,2,ldots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img96.png"
 ALT="$1, 2, \ldots$">|; 

$key = q/{displaymath}{{bf{h}(X_j)subseteqY_m;;;forallq_jinQ:lambda(q_j)=gamma_m,{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="247" HEIGHT="29" BORDER="0"
 SRC="|."$dir".q|img339.png"
 ALT="\begin{displaymath}
{\bf h}(X_j)\subseteq Y_m \;\;\;\forall q_j\in Q :
\lambda(q_j)=\gamma_m,
\end{displaymath}">|; 

$key = q/Theta(|Q|^{1slash2});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img308.png"
 ALT="$\Theta(\vert Q\vert^{1/2})$">|; 

$key = q/rs;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img87.png"
 ALT="$rs$">|; 

$key = q/y_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img104.png"
 ALT="$y_1$">|; 

$key = q/includegraphics[scale=0.8]{mealynet};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="245" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img145.png"
 ALT="\includegraphics[scale=0.8]{mealynet}">|; 

$key = q/lambda(q_j,sigma_k)inGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="93" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img44.png"
 ALT="$\lambda(q_j,\sigma_k)\in\Gamma$">|; 

$key = q/VcupSigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img267.png"
 ALT="$V\cup \Sigma$">|; 

$key = q/gamma_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img325.png"
 ALT="$\gamma_m$">|; 

$key = q/{epsilon};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img94.png"
 ALT="$\{\epsilon\}$">|; 

$key = q/Q={q_1,q_2,q_3,q_4};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="132" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img72.png"
 ALT="$Q=\{q_1,q_2,q_3,q_4\}$">|; 

$key = q/includegraphics[scale=0.8]{moorenet};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="335" HEIGHT="360" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img146.png"
 ALT="\includegraphics[scale=0.8]{moorenet}">|; 

$key = q/delta(q_1,1)=q_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img75.png"
 ALT="$\delta(q_1,1)=q_1$">|; 

$key = q/{{bf{y}[t]inY_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img326.png"
 ALT="${\bf y}[t]\in Y_m$">|; 

$key = q/W(|w|);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img392.png"
 ALT="$W(\vert w\vert)$">|; 

$key = q/q_I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img65.png"
 ALT="$q_I$">|; 

$key = q/Y_{calY};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img347.png"
 ALT="$Y_{\cal Y}$">|; 

$key = q/{displaymath}f_i({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_X}W_{ij}^{xx}x_j[t-1]+sum_{j=1}^{n_U}W_{ij}^{xu}u_j[t]+W^x_iright),{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="466" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img164.png"
 ALT="\begin{displaymath}f_i({\bf x}[t-1],{\bf u}[t])=g\left(\sum_{j=1}^{n_X} W_{ij}...
...j[t-1] +
\sum_{j=1}^{n_U} W_{ij}^{xu} u_j[t] +
W^x_i\right),
\end{displaymath}">|; 

$key = q/1leile{n_Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img199.png"
 ALT="$1\le i \le {n_Y}$">|; 

$key = q/2pbmodomega=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img362.png"
 ALT="$2p\bmod\omega=0$">|; 

$key = q/x_{i+kn_U}[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img186.png"
 ALT="$x_{i+kn_U}[t]$">|; 

$key = q/C(i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$C(i)$">|; 

$key = q/{{bf{x}_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img143.png"
 ALT="${\bf x}_0$">|; 

$key = q/sigma_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img256.png"
 ALT="$\sigma_m$">|; 

$key = q/g'();MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img215.png"
 ALT="$g'()$">|; 

$key = q/{displaymath}x_i[t]=thetaleft(b_i+sum_{jinC(i)}w_{ij}x_j[t-1]right),{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="249" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="\begin{displaymath}
x_i[t] =\theta\left( b_i + \sum_{j\in C(i)} w_{ij}
x_j[t-1]\right),
\end{displaymath}">|; 

$key = q/q_s'=sigma(1slash2+q_sslash2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="134" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img391.png"
 ALT="$q_s'=\sigma(1/2+q_s/2)$">|; 

$key = q/u[1]u[2]u[3]ldots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="103" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img107.png"
 ALT="$u[1]u[2]u[3]\ldots$">|; 

$key = q/Q={q_1,q_2,ldots,q_{|Q|}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="150" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img36.png"
 ALT="$Q=\{q_1,q_2,\ldots,q_{\vert Q\vert}\}$">|; 

$key = q/i=1,ldots,n_Y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img153.png"
 ALT="$i=1,\ldots,n_Y)$">|; 

$key = q/Y_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img335.png"
 ALT="$Y_m$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img260.png"
 ALT="$G$">|; 

$key = q/D=(X,U,V,{{bf{h},{{bf{d}_1,{{bf{d}_2,ldots,{{bf{d}_{V});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="223" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img246.png"
 ALT="$D=(X,U,V,{\bf h},{\bf d}_1,{\bf d}_2,\ldots,{\bf d}_{V})$">|; 

$key = q/n>n_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img293.png"
 ALT="$n&gt;n_0$">|; 

$key = q/lambda:QtimesSigmarightarrowGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img43.png"
 ALT="$\lambda:Q\times \Sigma\rightarrow \Gamma$">|; 

$key = q/K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img292.png"
 ALT="$K$">|; 

$key = q/{displaymath}y_i[t]=gleft(sum_{j=1}^{n_U}W_{ij}^{yu}u_j[t]+sum_{j=1}^{n_H}W_{ij}^{yx}x_j[t]+W^y_iright);,i=1ldotsn_Y.{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="425" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img238.png"
 ALT="\begin{displaymath}
y_i[t]=g\left(\sum_{j=1}^{n_U} W_{ij}^{yu} u_j[t]
+ \sum_{j=1}^{n_H} W_{ij}^{yx} x_j[t] + W^y_i \right)\;,
i=1\ldots n_Y.
\end{displaymath}">|; 

$key = q/{{bf{h}:XrightarrowU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img247.png"
 ALT="${\bf h}:X\rightarrow U$">|; 

$key = q/Y_{calN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img348.png"
 ALT="$Y_{\cal N}$">|; 

$key = q/g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img160.png"
 ALT="$g(x)$">|; 

$key = q/{{bf{h}:XrightarrowY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img141.png"
 ALT="${\bf h}:X\rightarrow Y$">|; 

$key = q/delta(q_j,sigma_k)=q_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img397.png"
 ALT="$\delta(q_j,\sigma_k)=q_i$">|; 

$key = q/{displaymath}h_i({{bf{x[t]})=x_{i+{n_U}n_I}[t],{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="147" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img198.png"
 ALT="\begin{displaymath}
h_i({\bf x[t]}) = x_{i+{n_U}n_I}[t],
\end{displaymath}">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img262.png"
 ALT="$S$">|; 

$key = q/{{bf{x}_0inX_I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="61" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img329.png"
 ALT="${\bf x}_0\in X_I$">|; 

$key = q/q_ineqq_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img320.png"
 ALT="$q_i\neq q_j$">|; 

$key = q/Y_mcapY_n=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="90" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img327.png"
 ALT="$Y_m\cap Y_n=\emptyset$">|; 

$key = q/lambda:QrightarrowGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img60.png"
 ALT="$\lambda:Q\rightarrow\Gamma$">|; 

$key = q/log(n_X);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img359.png"
 ALT="$\log(n_X)$">|; 

$key = q/q_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img317.png"
 ALT="$q_i$">|; 

$key = q/emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img91.png"
 ALT="$\emptyset$">|; 

$key = q/QtimesPhi^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img285.png"
 ALT="$Q\times \Phi^*$">|; 

$key = q/F={q_1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img71.png"
 ALT="$F=\{q_1\}$">|; 

$key = q/{{bf{z}({{bf{x}[t-1],{{bf{u}[t]);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img168.png"
 ALT="${\bf z}({\bf x}[t-1],{\bf u}[t])$">|; 

$key = q/u[1]u[2];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img106.png"
 ALT="$u[1]u[2]$">|; 

$key = q/gamma_m=lambda(q_j,sigma_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img336.png"
 ALT="$\gamma_m=\lambda(q_j,\sigma_k)$">|; 

$key = q/{{bf{u}_kinU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img253.png"
 ALT="${\bf u}_k\in U$">|; 

$key = q/{{bf{d}_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img254.png"
 ALT="${\bf d}_i$">|; 

$key = q/{displaymath}Sigma={{{tt{a},{{tt{m},{{tt{o},{{tt{t}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img274.png"
 ALT="\begin{displaymath}\Sigma = \{ {\tt a}, {\tt m}, {\tt o}, {\tt t} \}\end{displaymath}">|; 

$key = q/X_isubseteqX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img316.png"
 ALT="$X_i\subseteq X$">|; 

$key = q/Gamma={{{tt{E},{{tt{O}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img50.png"
 ALT="$\Gamma=\{{\tt E}, {\tt O}\}$">|; 

$key = q/y_i[t-k];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img189.png"
 ALT="$y_i[t-k]$">|; 

$key = q/{{bf{h}:XtimesUrightarrowY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="112" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img138.png"
 ALT="${\bf h}:X
\times U \rightarrow Y$">|; 

$key = q/{displaymath}y_i[t]=x_i[t];;;i=1,ldots,n_Y;;;n_Y<n_X{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="263" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img350.png"
 ALT="\begin{displaymath}
y_i[t]=x_i[t];\;\;i=1,\ldots, n_Y;\;\; n_Y&lt;n_X
\end{displaymath}">|; 

$key = q/{displaymath}z_i[t]=gleft(sum_{j=n_U+1}^{n_X}W_{ij}^{zx}x_i[t-1]+sum_{j=1}^{n_U}W_{ij}^{zu}u_j[t]+W_i^zright),;1leilen_Z.{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="472" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img197.png"
 ALT="\begin{displaymath}
z_i[t] = g\left(\sum_{j=n_U+1}^{n_X} W_{ij}^{zx} x_i[t-1] +
...
...^{n_U} W_{ij}^{zu} u_j[t] + W_i^z \right),\;
1 \le i \le n_Z.
\end{displaymath}">|; 

$key = q/qinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img371.png"
 ALT="$q\in F$">|; 

$key = q/n_H+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img237.png"
 ALT="$n_H+1$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img86.png"
 ALT="$s$">|; 

$key = q/n_In_U+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img188.png"
 ALT="$n_In_U+1$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img374.png"
 ALT="$w$">|; 

$key = q/Sigma={sigma_1,sigma_2ldots,sigma_{|Sigma|}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="134" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img252.png"
 ALT="$\Sigma={\sigma_1,\sigma_2\ldots,\sigma_{\vert\Sigma\vert}}$">|; 

$key = q/delta:QtimesLambdarightarrowQtimesLambdatimes{L,R};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="201" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img366.png"
 ALT="$\delta:Q\times\Lambda\rightarrow Q\times\Lambda\times\{L,R\}$">|; 

$key = q/{displaymath}lambda(q_i,sigma_k)=array{{l}{calY}mbox{if}delta(q_i,sigma_k)inF{calN}mbox{otherwise.}array{{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="207" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img346.png"
 ALT="\begin{displaymath}
\lambda(q_i,\sigma_k) =\begin{array}{l}
{\cal Y} \mbox{ if...
...q_i,\sigma_k)\in F \\\\
{\cal N} \mbox{ otherwise.}
\end{array}\end{displaymath}">|; 

$key = q/{displaymath}V={S,A}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img273.png"
 ALT="\begin{displaymath}V = \{ S, A \}\end{displaymath}">|; 

$key = q/{displaymath}z_i(({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_X}W_{ij}^{zx}x_j[t-1]+sum_{j=1}^{n_U}W_{ij}^{zu}u_j[t]+W_i^zright),{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="448" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img169.png"
 ALT="\begin{displaymath}
z_i(({\bf x}[t-1],{\bf u}[t]) = g\left( \sum_{j=1}^{n_X} W_{...
...j[t-1]
+ \sum_{j=1}^{n_U} W_{ij}^{zu} u_j[t]
+ W_i^z\right),
\end{displaymath}">|; 

$key = q/{{bf{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img192.png"
 ALT="${\bf f}$">|; 

$key = q/-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img151.png"
 ALT="$-1$">|; 

$key = q/X_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img331.png"
 ALT="$X_i$">|; 

$key = q/U_ksubseteqU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img322.png"
 ALT="$U_k\subseteq U$">|; 

$key = q/{{bf{x}'[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img398.png"
 ALT="${\bf x}'[t]$">|; 

$key = q/t=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$t=0$">|; 

$key = q/{displaymath}M=(Q,{0,1},{0,1,B},delta,q_1,B,{q_2}){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="267" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img379.png"
 ALT="\begin{displaymath}M=(Q,\{0,1\},\{0,1,B\},\delta,q_1,B,\{q_2\})\end{displaymath}">|; 

$key = q/delta(q_j,sigma_k)inQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="94" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img42.png"
 ALT="$\delta(q_j,\sigma_k)\in Q$">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img264.png"
 ALT="$\alpha$">|; 

$key = q/f(n)geQF(n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img296.png"
 ALT="$f(n)\ge
QF(n)$">|; 

$key = q/I-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img33.png"
 ALT="$I-1$">|; 

$key = q/n_Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img195.png"
 ALT="$n_Z$">|; 

$key = q/{displaymath}P=left{array{{rrcl}(1)&S&rightarrow&{{tt{to}A{{tt{to}(2)&A&rightarrow&{{tt{ma}(3)&A&rightarrow&{{tt{ma}Aarray{right}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="217" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img275.png"
 ALT="\begin{displaymath}P = \left\{ \begin{array}{rrcl}
(1) &amp; S &amp; \rightarrow &amp; {\tt...
...a} \\\\
(3) &amp; A &amp; \rightarrow &amp; {\tt ma}A
\end{array} \right\}\end{displaymath}">|; 

$key = q/O(|Q|);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img287.png"
 ALT="$O(\vert Q\vert)$">|; 

$key = q/n_X=|Q|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img313.png"
 ALT="$n_X=\vert Q\vert$">|; 

$key = q/{displaymath}p_{{rm{new}=p_{{rm{old}-alpha_pfrac{partialE}{partialp}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="140" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img209.png"
 ALT="\begin{displaymath}
p_{\rm new}=p_{\rm old} - \alpha_p \frac{\partial E}{\partial p}
\end{displaymath}">|; 

$key = q/{displaymath}lambda(q_i)=array{{l}{calY}mbox{if}q_iinF{calN}mbox{otherwise,}array{{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="154" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img345.png"
 ALT="\begin{displaymath}
\lambda(q_i) =\begin{array}{l} {\cal Y} \mbox{ if } q_i\in F \\\\
{\cal N} \mbox{ otherwise,}
\end{array}\end{displaymath}">|; 

$key = q/{mathbb{R}{;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img243.png"
 ALT="$\mathbb{R}$">|; 

$key = q/{displaymath}{{bf{y}[t]=N({{bf{u}[t],{{bf{u}[t-1],ldots,{{bf{u}[t-n_I],{{bf{y}[t-1],{{bf{y}[t-2],ldots,{{bf{y}[t-n_O]).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="475" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img200.png"
 ALT="\begin{displaymath}
{\bf y}[t]=N({\bf u}[t],{\bf u}[t-1],\ldots,{\bf u}[t-n_I],{\bf y}[t-1],{\bf y}[t-2],\ldots,{\bf y}[t-n_O]).
\end{displaymath}">|; 

$key = q/{-1,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img309.png"
 ALT="$\{-1,1\}$">|; 

$key = q/U=[S_0,S_1]^{n_U};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img244.png"
 ALT="$U=[S_0,S_1]^{n_U}$">|; 

$key = q/delta(q_2,1)=q_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img54.png"
 ALT="$\delta(q_2,1)=q_1$">|; 

$key = q/{mathbb{R}{^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img343.png"
 ALT="$\mathbb{R}^n$">|; 

$key = q/betain(VcupSigma)^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="95" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img270.png"
 ALT="$\beta\in (V\cup\Sigma)^*$">|; 

$key = q/{displaymath}array{{rlc}f_i({{bf{x}[t-1],{{bf{u}[t])=&u_i[t],&1leile{n_U};f_i({{,{{bf{u}[t])=&x_{i-{n_U}}[t-1],&{n_U}<ile{n_U}n_Iarray{{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="349" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img193.png"
 ALT="\begin{displaymath}
\begin{array}{rlc}
f_i({\bf x}[t-1],{\bf u}[t]) =&amp; u_i[t], &amp;...
...]) =&amp; x_{i-{n_U}}[t-1], &amp; {n_U} &lt;i\le {n_U} n_I
\\\\
\end{array}\end{displaymath}">|; 

$key = q/u[1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img105.png"
 ALT="$u[1]$">|; 

$key = q/dollar;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img382.png"
 ALT="$\$$">|; 

$key = q/x[0];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img119.png"
 ALT="$x[0]$">|; 

$key = q/-Hslash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img357.png"
 ALT="$-H/2$">|; 

$key = q/Sigma={sigma_1,sigma_2,ldots,sigma_{|Sigma|}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="154" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img37.png"
 ALT="$\Sigma=\{\sigma_1,\sigma_2,\ldots,\sigma_{\vert\Sigma\vert}\}$">|; 

$key = q/u_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$u_2$">|; 

$key = q/x>1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img387.png"
 ALT="$x&gt;1$">|; 

$key = q/{displaymath}y[t]=h(x[t-1],u[t]).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="151" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img118.png"
 ALT="\begin{displaymath}
y[t]=h(x[t-1],u[t]).
\end{displaymath}">|; 

$key = q/{sigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img92.png"
 ALT="$\{\sigma\}$">|; 

$key = q/FsubsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img370.png"
 ALT="$F\subset Q$">|; 

$key = q/{{bf{W}^{xu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img299.png"
 ALT="${\bf W}^{xu}$">|; 

$key = q/{displaymath}D(x)=left{array{{rcl}0.8&mbox{if}&x>0.50.2&mbox{otherwise}&array{right.,{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="261" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img402.png"
 ALT="\begin{displaymath}D(x)=\left\{\begin{array}{rcl} 0.8&amp;\mbox{ if }&amp;x&gt;0.5\\\\
0.2&amp;\mbox{otherwise}&amp;
\end{array}\right.,\end{displaymath}">|; 

$key = q/O((|Q|)^{3slash4});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img305.png"
 ALT="$O((\vert Q\vert)^{3/4})$">|; 

$key = q/2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img363.png"
 ALT="$2$">|; 

$key = q/Q={q_1,q_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img47.png"
 ALT="$Q=\{q_1,q_2\}$">|; 

$key = q/{{bf{x}[0];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img144.png"
 ALT="${\bf x}[0]$">|; 

$key = q/n_In_U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img183.png"
 ALT="$n_In_U$">|; 

$key = q/sigmainSigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$\sigma \in \Sigma $">|; 

$key = q/{displaymath}frac{partialx_i[t]}{partialW^{xx}_{kl}}=g'(Xi_i[t])left(delta_{ik}xj}^{xx}frac{partialx_j[t-1]}{partialW^{xx}_{kl}}right),{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="354" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img214.png"
 ALT="\begin{displaymath}
\frac{\partial x_i[t]}{\partial W^{xx}_{kl}}=g'(\Xi_i[t])
\l...
...^{xx}
\frac{\partial x_j[t-1]}{\partial W^{xx}_{kl}}
\right),
\end{displaymath}">|; 

$key = q/i=1,ldots,n_Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="94" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img170.png"
 ALT="$i=1,\ldots,n_Z$">|; 

$key = q/{displaymath}f_{i+{n_U}n_i}({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_Z}W_{ij}^{xz}z_j[t]+W_i^xright),;1leilen_Y{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="429" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img196.png"
 ALT="\begin{displaymath}
f_{i+{n_U}n_i}({\bf x}[t-1],{\bf u}[t]) =
g\left(\sum_{j=1}^{n_Z} W_{ij}^{xz} z_j[t] + W_i^x \right),\; 1
\le i \le n_Y
\end{displaymath}">|; 

$key = q/X_epsilon^{(i)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img255.png"
 ALT="$X_\epsilon^{(i)}$">|; 

$key = q/{{bf{W}-muE({{bf{W}+{{bf{pi})-E({{bf{W})){{bf{pi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img225.png"
 ALT="${\bf W}-\mu E({\bf W}+{\bf\pi})-E({\bf W})){\bf\pi}$">|; 

$key = q/q_IinQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img45.png"
 ALT="$q_I\in Q$">|; 

$key = q/{{bf{h}(A)={{{bf{h}({{bf{x}):{{bf{x}inA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="162" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img340.png"
 ALT="${\bf h}(A)=\{{\bf h}({\bf x}) : {\bf x}\in A\}$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img373.png"
 ALT="$B$">|; 

$key = q/alphain(VcupSigma)^*V(VcupSigma)^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="170" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img268.png"
 ALT="$\alpha\in
(V\cup\Sigma)^*V(V\cup\Sigma)^*$">|; 

$key = q/includegraphics[scale=0.8]{fig0110};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="242" HEIGHT="116" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img59.png"
 ALT="\includegraphics[scale=0.8]{fig0110}">|; 

$key = q/{displaymath}h_{i}({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_Z}W_{ij}^{yz}z_j[t]+W_i^yright),;1leilen_Y{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="390" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img204.png"
 ALT="\begin{displaymath}
h_{i}({\bf x}[t-1],{\bf u}[t]) =
g\left(\sum_{j=1}^{n_Z} W_{ij}^{yz} z_j[t] + W_i^y \right),\; 1
\le i \le n_Y
\end{displaymath}">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img69.png"
 ALT="$F$">|; 

$key = q/n_X=n_In_U+n_On_Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="144" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img182.png"
 ALT="$n_X=n_In_U+n_On_Y$">|; 

$key = q/{{bf{h}({{bf{x}[t]);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img174.png"
 ALT="${\bf h}({\bf x}[t])$">|; 

$key = q/{{bf{h}({{bf{x}[t-1],{{bf{u}[t]);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="110" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img165.png"
 ALT="${\bf h}({\bf x}[t-1],{\bf u}[t])$">|; 

$key = q/U_minU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img257.png"
 ALT="$U_m\in U$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img122.png"
 ALT="$N$">|; 

$key = q/includegraphics{tdnn};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="303" HEIGHT="374" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img207.png"
 ALT="\includegraphics{tdnn}">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img367.png"
 ALT="$R$">|; 

$key = q/g_L(x)=1slash(1+exp(-x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="178" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img161.png"
 ALT="$g_L(x)=1/(1+\exp(-x))$">|; 

$key = q/V;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img241.png"
 ALT="$V$">|; 

$key = q/Theta(|Q||Sigma|);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="71" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img297.png"
 ALT="$\Theta(\vert Q\vert\vert\Sigma\vert)$">|; 

$key = q/Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img240.png"
 ALT="$Z$">|; 

$key = q/k=1ldotsn_O;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img190.png"
 ALT="$k=1\ldots n_O$">|; 

$key = q/{displaymath}y[t]=h(x[t]),{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="93" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img117.png"
 ALT="\begin{displaymath}
y[t]=h(x[t]),
\end{displaymath}">|; 

$key = q/BnotinSigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img369.png"
 ALT="$B\not\in\Sigma$">|; 

$key = q/q_s'=sigma(2q_s-1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img390.png"
 ALT="$q_s'=\sigma(2q_s-1)$">|; 

$key = q/F:mathbb{N}rightarrowmathbb{R}^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="88" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img289.png"
 ALT="$F:\mathbb{N}\rightarrow\mathbb{R}^+$">|; 

$key = q/{displaymath}x_i[t]=gleft(sum_{j=1}^{n_U}W_{ij}^{xu}u_j[t]+W_{ii}^{xx}x_i[t-1]+W^x_iright){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="327" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img232.png"
 ALT="\begin{displaymath}
x_i[t]=g\left(\sum_{j=1}^{n_U} W_{ij}^{xu} u_j[t] + W_{ii}^{xx}
x_i[t-1] +
W^x_i \right)
\end{displaymath}">|; 

$key = q/{displaymath}z_i[t]=gleft(sum_{j=1}^{n_X}W_{ij}^{zx}x_i[t-1]+sum_{j=1}^{n_U}W_{ij}^{zu}u_j[t]+W_i^zright),;1leilen_Z.{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="447" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img205.png"
 ALT="\begin{displaymath}
z_i[t] = g\left(\sum_{j=1}^{n_X} W_{ij}^{zx} x_i[t-1] +
\su...
...^{n_U} W_{ij}^{zu} u_j[t] + W_i^z \right),\;
1 \le i \le n_Z.
\end{displaymath}">|; 

$key = q/theta(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$\theta(x)$">|; 

$key = q/u_i[t-k];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="61" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img184.png"
 ALT="$u_i[t-k]$">|; 

$key = q/q_iinQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img315.png"
 ALT="$q_i \in Q$">|; 

$key = q/E({{bf{W});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img224.png"
 ALT="$E({\bf W})$">|; 

$key = q/FsubseteqQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img66.png"
 ALT="$F\subseteq Q$">|; 

$key = q/{displaymath}{{bf{y}[t]=N({{bf{u}[t],{{bf{u}[t-1],ldots,{{bf{u}[t-n_I]).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="257" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img206.png"
 ALT="\begin{displaymath}
{\bf y}[t]=N({\bf u}[t],{\bf u}[t-1],\ldots,{\bf u}[t-n_I]).
\end{displaymath}">|; 

$key = q/{{bf{f}:XtimesUrightarrowX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="110" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img132.png"
 ALT="${\bf f}: X \times U \rightarrow X$">|; 

$key = q/{displaymath}M=(Q,Sigma,delta,q_I,F){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img61.png"
 ALT="\begin{displaymath}
M=(Q,\Sigma,\delta,q_I,F)
\end{displaymath}">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img120.png"
 ALT="$f$">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$j$">|; 

$key = q/alpharightarrowbeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img266.png"
 ALT="$\alpha\rightarrow\beta$">|; 

$key = q/xu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img158.png"
 ALT="$xu$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="$n$">|; 

$key = q/{{bf{pi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img222.png"
 ALT="${\bf\pi}$">|; 

$key = q/x_{n_In_U+i+(k-1)n_Y}[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img191.png"
 ALT="$x_{n_In_U+i+(k-1)n_Y}[t]$">|; 

$key = q/lambda(q_1,1)={{tt{O};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img56.png"
 ALT="$\lambda(q_1,1)={\tt O}$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img85.png"
 ALT="$r$">|; 

$key = q/0000ldots011ldots10000ldots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="168" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img377.png"
 ALT="$0000\ldots011\ldots10000\ldots$">|; 

$key = q/n_I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img180.png"
 ALT="$n_I$">|; 

$key = q/delta(q_3,1)=q_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img78.png"
 ALT="$\delta(q_3,1)=q_1$">|; 

$key = q/epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img68.png"
 ALT="$\epsilon$">|; 

$key = q/|Gamma|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img84.png"
 ALT="$\vert\Gamma\vert$">|; 

$key = q/includegraphics[scale=0.8]{fig017};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="228" HEIGHT="116" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img81.png"
 ALT="\includegraphics[scale=0.8]{fig017}">|; 

$key = q/n_Ylen_X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img177.png"
 ALT="$n_Y\le
n_X$">|; 

$key = q/{displaymath}f_i({{bf{x}[t-1],{{bf{u}[t])=x_{i-{n_Y}}[t-1],;;{n_U}n_I+{n_Y}<ile{n_X};{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="381" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img194.png"
 ALT="\begin{displaymath}
f_i({\bf x}[t-1],{\bf u}[t]) = x_{i-{n_Y}}[t-1],\;\; {n_U}n_I+{n_Y} &lt;
i\le {n_X};
\end{displaymath}">|; 

$key = q/n=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="$n=2$">|; 

$key = q/i=1,ldotsV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img249.png"
 ALT="$i=1,\ldots V$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img226.png"
 ALT="$\mu$">|; 

$key = q/n_U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img129.png"
 ALT="$n_U$">|; 

$key = q/n_Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img131.png"
 ALT="$n_Y$">|; 

$key = q/x_i[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img227.png"
 ALT="$x_i[t]$">|; 

$key = q/j<i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img307.png"
 ALT="$j&lt;i$">|; 

$key = q/{displaymath}M=(Q,Sigma,Gamma,delta,lambda,q_I){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="149" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img35.png"
 ALT="\begin{displaymath}
M=(Q,\Sigma,\Gamma,\delta,\lambda,q_I)
\end{displaymath}">|; 

$key = q/|Q||Sigma|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img83.png"
 ALT="$\vert Q\vert\vert\Sigma\vert$">|; 

$key = q/n_X^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img213.png"
 ALT="$n_X^2$">|; 

$key = q/-H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img356.png"
 ALT="$-H$">|; 

$key = q/y=y[1]y[2]ldotsy[L_y];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="144" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img100.png"
 ALT="$y=y[1]y[2]\ldots y[L_y]$">|; 

$key = q/F={q_1,q_2,q_3};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img73.png"
 ALT="$F=\{q_1,q_2,q_3\}$">|; 

$key = q/x_i[t]=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$x_i[t]=1$">|; 

$key = q/Y=[S_0,S_1]^{n_Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="106" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img130.png"
 ALT="$Y=[S_0,S_1]^{n_Y}$">|; 

$key = q/{bfx}[0];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img219.png"
 ALT="${\bf x}[0]$">|; 

$key = q/w_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$w_{ij}$">|; 

$key = q/Phi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img282.png"
 ALT="$\Phi$">|; 

$key = q/Lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img365.png"
 ALT="$\Lambda$">|; 

$key = q/f(n)+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img378.png"
 ALT="$f(n)+1$">|; 

$key = q/{{bf{x}[t]=(1-alpha){{bf{x}'[t]+alpha{{bf{c}[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="181" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img400.png"
 ALT="${\bf x}[t]=(1-\alpha){\bf x}'[t]+\alpha {\bf c}[t]$">|; 

$key = q/u[1]u[2]ldotsu[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img108.png"
 ALT="$u[1]u[2]\ldots u[t]$">|; 

$key = q/u_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$u_1$">|; 

$key = q/{0,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img380.png"
 ALT="$\{0,1\}$">|; 

$key = q/{displaymath}y[t]=x_1[t]{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img384.png"
 ALT="\begin{displaymath}y[t]=x_1[t]\end{displaymath}">|; 

$key = q/winSigma^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img372.png"
 ALT="$w\in\Sigma^*$">|; 

$key = q/g(x)=1slash(1+exp(-x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="169" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img354.png"
 ALT="$g(x)=1/(1+\exp(-x))$">|; 

$key = q/{displaymath}{{bf{y}[t]={{bf{h}({{bf{x}[t]);{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img142.png"
 ALT="\begin{displaymath}
{\bf y}[t]={\bf h}({\bf x}[t]);
\end{displaymath}">|; 

$key = q/{displaymath}f_i({{bf{x}[t-1],{{bf{u}[t])=gleft(sum_{j=1}^{n_X}sum_{k=1}^{n_U}W^{xxu}_{ijk}x_j[t-1]u_k[t]+W^x_iright),{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="401" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img148.png"
 ALT="\begin{displaymath}
f_i({\bf x}[t-1],{\bf u}[t])=
g\left( \sum_{j=1}^{n_X} \sum_{k=1}^{n_U} W^{xxu}_{ijk} x_j[t-1] u_k[t] +
W^x_i \right),
\end{displaymath}">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img162.png"
 ALT="$1$">|; 

$key = q/Sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img63.png"
 ALT="$\Sigma$">|; 

$key = q/lambda(q_2,0)={{tt{O};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img57.png"
 ALT="$\lambda(q_2,0)={\tt O}$">|; 

$key = q/Sigma=Gamma={0,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="107" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img303.png"
 ALT="$\Sigma=\Gamma=\{0,1\}$">|; 

$key = q/includegraphics{xor};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="339" HEIGHT="152" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img31.png"
 ALT="\includegraphics{xor}">|; 

$key = q/alpha_p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img210.png"
 ALT="$\alpha_p$">|; 

$key = q/{displaymath}SRightarrow_1{{tt{to}A{{tt{to}Rightarrow_3{{tt{toma}A{{tt{to}Rightarrow_3{{tt{tomama}A{{tt{to}Rightarrow_2{{tt{tomamamato}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="408" HEIGHT="27" BORDER="0"
 SRC="|."$dir".q|img277.png"
 ALT="\begin{displaymath}S \Rightarrow_1 {\tt to}A{\tt to} \Rightarrow_3 {\tt toma}A{\...
...htarrow_3 {\tt tomama}A{\tt to}
\Rightarrow_2 {\tt tomamamato}\end{displaymath}">|; 

$key = q/N=(X,U,Y,{{bf{f},{{bf{h},{{bf{x}_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="156" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img314.png"
 ALT="$N=(X,U,Y,{\bf f},{\bf h},{\bf x}_0)$">|; 

$key = q/M=(Q,Sigma,delta,q_I,F);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="141" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img70.png"
 ALT="$M=(Q,\Sigma,\delta,q_I,F)$">|; 

$key = q/2^{(2^n)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="$2^{(2^n)}$">|; 

$key = q/i=1,ldots,n_Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="95" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img166.png"
 ALT="$i=1,\ldots,n_Y$">|; 

$key = q/sigma_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img41.png"
 ALT="$\sigma_k$">|; 

$key = q/Gamma={{calY},{calN}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img344.png"
 ALT="$\Gamma=\{{\cal Y},{\cal N}\}$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$A$">|; 

$key = q/U_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img258.png"
 ALT="$U_m$">|; 

$key = q/{displaymath}G=(V,Sigma,P,S).{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="115" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img263.png"
 ALT="\begin{displaymath}G=(V,\Sigma,P,S).\end{displaymath}">|; 

$key = q/x>=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$x&gt;=0$">|; 

$key = q/h_i({{bf{x}[t])=x_i[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img178.png"
 ALT="$h_i({\bf x}[t])=x_i[t]$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img211.png"
 ALT="$E$">|; 

$key = q/y[t];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img113.png"
 ALT="$y[t]$">|; 

$key = q/{displaymath}N=(X,U,Y,{{bf{f},{{bf{h},{{bf{x}_0){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="151" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img123.png"
 ALT="\begin{displaymath}
N=(X,U,Y,{\bf f},{\bf h},{\bf x}_0)
\end{displaymath}">|; 

$key = q/{displaymath}Xi_i[t]=sum_{j=1}^{n_X}W_{ij}^{xx}x_j[t-1]+sum_{j=1}^{n_U}W_{ij}^{xu}u_j[t]+W^x_i{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="319" HEIGHT="56" BORDER="0"
 SRC="|."$dir".q|img218.png"
 ALT="\begin{displaymath}
\Xi_i[t]=
\sum_{j=1}^{n_X} W_{ij}^{xx} x_j[t-1] +
\sum_{j=1}^{n_U} W_{ij}^{xu} u_j[t] +
W^x_i
\end{displaymath}">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img32.png"
 ALT="$I$">|; 

$key = q/gamma_minGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img323.png"
 ALT="$\gamma_m \in \Gamma$">|; 

$key = q/Y_msubseteqY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img324.png"
 ALT="$Y_m\subseteq Y$">|; 

$key = q/O(log|Q|);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img302.png"
 ALT="$O(\log \vert Q\vert)$">|; 

$key = q/i=1ldotsn_C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img233.png"
 ALT="$i=1\ldots n_C$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$M$">|; 

$key = q/|Q||Sigma|+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img351.png"
 ALT="$\vert Q\vert\vert\Sigma\vert+1$">|; 

$key = q/S_1=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img152.png"
 ALT="$S_1=1$">|; 

$key = q/Q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img62.png"
 ALT="$Q$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img93.png"
 ALT="$\sigma$">|; 

$key = q/M=(Q,Sigma,Lambda,delta,q_I,B,F);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="179" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img364.png"
 ALT="$M=(Q,\Sigma,\Lambda,\delta,q_I,B,F)$">|; 

$key = q/k=0ldotsn_I-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="110" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img185.png"
 ALT="$k=0\ldots n_I-1$">|; 

$key = q/{displaymath}x_i[t]=gleft(sum_{j=1}^{n_U}W_{ij}^{xu}u_j[t]+W_{ii}^{xx}x_i[t-1]+sum_{j=1}^{n_H}W_{ij}^{xx'}x_j[t]+W^x_iright){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="439" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img235.png"
 ALT="\begin{displaymath}
x_i[t]=g\left(\sum_{j=1}^{n_U} W_{ij}^{xu} u_j[t] + W_{ii}^{...
...i[t-1] +
\sum_{j=1}^{n_H} W_{ij}^{xx'} x_j[t] + W^x_i \right)
\end{displaymath}">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img95.png"
 ALT="$U$">|; 

$key = q/E=(X,U,V,{{bf{f});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img242.png"
 ALT="$E=(X,U,V,{\bf f})$">|; 

$key = q/W^y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img159.png"
 ALT="$W^y_i$">|; 

$key = q/theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="$\theta$">|; 

$key = q/Srightarrowepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img279.png"
 ALT="$S\rightarrow\epsilon$">|; 

$key = q/r{{tt{|}s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img88.png"
 ALT="$r{\tt \vert}s$">|; 

$key = q/Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img99.png"
 ALT="$Y$">|; 

$key = q/delta(q_3,0)=q_4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img77.png"
 ALT="$\delta(q_3,0)=q_4$">|; 

1;

