# LaTeX2HTML 99.2beta6 (1.42)
# Associate internals original text with physical files.


$key = q/eq:fk/;
$ref_files{$key} = "$dir".q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/se:wfcnc/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:neuMo/;
$ref_files{$key} = "$dir".q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_jordan86t/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_stolcke92tr/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/s3:notall/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Alo91a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_goudreau94a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/s3:LBA/;
$ref_files{$key} = "$dir".q|node64.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_mitra93b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_das91j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_cid93p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:acm/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_williams89j1/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_williams89j2/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_box94b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chovan96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_ortiz97p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_mozer.89.A/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_siegelmann96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_wang57j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_rumelhart86b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_cid94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Mozer93p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kechriotis94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:OQGI/;
$ref_files{$key} = "$dir".q|node73.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_ifeachor94b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kohavi78b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:pollack/;
$ref_files{$key} = "$dir".q|node84.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_cauwenberghs96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_pollack90j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:newrobhi1/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:newrobhi2/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_NC:Williams89/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_lettvin59j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:hk/;
$ref_files{$key} = "$dir".q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Cle89a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_aussem96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_omlin96j2/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_NC:Watrous92/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_omlin96j3/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_giles95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_draye95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:Mealy/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_adali97j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_solomonoff64j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chen95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_moore56j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/fg:TDNN/;
$ref_files{$key} = "$dir".q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_pollack91j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:dynxxu/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/se:rulex/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/se:GI/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:Moore/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_stiles97j1/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_stiles97j2/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_parisi97j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sima97j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:DTRNNGI/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:zJordan/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_neco97p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:GIpapers/;
$ref_files{$key} = "$dir".q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:Horne/;
$ref_files{$key} = "$dir".q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_salomaa73b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bengio94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chomsky65b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_clouse94t/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:Kohonen/;
$ref_files{$key} = "$dir".q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:GIFSM/;
$ref_files{$key} = "$dir".q|node82.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:mealydef/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_das94p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/s3:BPTT/;
$ref_files{$key} = "$dir".q|node28.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_carrasco00j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_alquezar95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:robinsonfi/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_watrous90j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_minsky56b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sluijter95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_puskorius94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_zbikowski95b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_frasconi96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:DTRNNSP/;
$ref_files{$key} = "$dir".q|node25.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:corst/;
$ref_files{$key} = "$dir".q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_oppenheim89b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chen97j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:dynyxu/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_markov58j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:RAAM/;
$ref_files{$key} = "$dir".q|node40.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_tino95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_gori89p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_manolios94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:DTRNNFSM/;
$ref_files{$key} = "$dir".q|node47.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_siu95b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:Zeng/;
$ref_files{$key} = "$dir".q|node92.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_hush93a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_clouse97j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_connor94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:robinsonhi/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:DFA/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_hubel59j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_forcada95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_mcculloch59b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_NC:Giles92/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_clouse97p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_gori98j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/fg:moorenet/;
$ref_files{$key} = "$dir".q|node21.html|; 
$noresave{$key} = "$nosave";

$key = q/se:McCPnlc/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:Angeline/;
$ref_files{$key} = "$dir".q|node26.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Wai89a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:kremer/;
$ref_files{$key} = "$dir".q|node49.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_horne96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sejnowski87j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Hop79a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sperduti95j2/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_McClelland86a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:clus/;
$ref_files{$key} = "$dir".q|node79.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_wang95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:BPTT/;
$ref_files{$key} = "$dir".q|node28.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_minsky67b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_pearlmutter95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_li95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_fahlman-91/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:gaCh/;
$ref_files{$key} = "$dir".q|node44.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_vonneumann56b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_das98j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:DFA/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_minsky59p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_burks57j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Kohonen74/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_robinson91j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:GIPDA/;
$ref_files{$key} = "$dir".q|node89.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chiu95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_elman91j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/fg:mealynet/;
$ref_files{$key} = "$dir".q|node21.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_forcada01b2/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Gil90a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_wang96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sperduti94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_mccarthy56b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:LP/;
$ref_files{$key} = "$dir".q|node34.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_pineda87j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_turing36j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ch:CCDTRNN/;
$ref_files{$key} = "$dir".q|node42.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_forcada97p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:RRBF/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bullock65b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_dertouzos65b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_williams92p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kremer95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_nerrand94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:SPDTRNNpapers/;
$ref_files{$key} = "$dir".q|node37.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_blair97j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_rashevsky40b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sperduti95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bulsari95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_hopfield/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_minsky69b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:TMDTRNN/;
$ref_files{$key} = "$dir".q|node59.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_parberry94b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:Alon/;
$ref_files{$key} = "$dir".q|node50.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:outMealy/;
$ref_files{$key} = "$dir".q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/fg:xor/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:RLS/;
$ref_files{$key} = "$dir".q|node30.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kremer96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/s3:RTRL/;
$ref_files{$key} = "$dir".q|node29.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:rpsig/;
$ref_files{$key} = "$dir".q|node55.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kremer96p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chrisman91j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:FSMsigDTRNN/;
$ref_files{$key} = "$dir".q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:chohie/;
$ref_files{$key} = "$dir".q|node46.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:coroumealy/;
$ref_files{$key} = "$dir".q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:Ljung/;
$ref_files{$key} = "$dir".q|node25.html|; 
$noresave{$key} = "$nosave";

$key = q/se:FARL/;
$ref_files{$key} = "$dir".q|node17.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:coroumoore/;
$ref_files{$key} = "$dir".q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Wer74a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_McC43a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:defRAAM/;
$ref_files{$key} = "$dir".q|node40.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:TM/;
$ref_files{$key} = "$dir".q|node60.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:stagen/;
$ref_files{$key} = "$dir".q|node74.html|; 
$noresave{$key} = "$nosave";

$key = q/ch:SPNN/;
$ref_files{$key} = "$dir".q|node18.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_hebb49b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_robinson94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Chalmers1990/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bridle90j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_werbos90j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:gba/;
$ref_files{$key} = "$dir".q|node27.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:kolen/;
$ref_files{$key} = "$dir".q|node77.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kuhn90j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_zeng93j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:cleeremans/;
$ref_files{$key} = "$dir".q|node83.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_weigend93b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_williams95b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/s3:EKF/;
$ref_files{$key} = "$dir".q|node30.html|; 
$noresave{$key} = "$nosave";

$key = q/se:flt/;
$ref_files{$key} = "$dir".q|node43.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:SBSP/;
$ref_files{$key} = "$dir".q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kwasny95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_dreider95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_siegelmann91j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_tsoi97j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/fg:odd/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_janacek93b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_perrin90b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_gilbert54j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_haykin95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_wu94tr/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_carrasco96b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kleene56b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_tomita82p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_zeng94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_casey96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:rpsig/;
$ref_files{$key} = "$dir".q|node58.html|; 
$noresave{$key} = "$nosave";

$key = q/se:dtrnn/;
$ref_files{$key} = "$dir".q|node21.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_hertz91b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:nsm/;
$ref_files{$key} = "$dir".q|node21.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_cheng95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bradley95p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:LADTRNN/;
$ref_files{$key} = "$dir".q|node26.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kremer99j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_qian88j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:nohidden/;
$ref_files{$key} = "$dir".q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/fg:tomita4/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:tino/;
$ref_files{$key} = "$dir".q|node87.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:elmanfi/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Elm90a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_rosenblatt62b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:next/;
$ref_files{$key} = "$dir".q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Nar90a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_shannon49j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_unnikrishnan94j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:RTRL/;
$ref_files{$key} = "$dir".q|node29.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:Jordannet/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/s3:RCC/;
$ref_files{$key} = "$dir".q|node33.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:Casey/;
$ref_files{$key} = "$dir".q|node53.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_copi58j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:leapre/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/se:ncfa/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_kolen94p2/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:RL/;
$ref_files{$key} = "$dir".q|node72.html|; 
$noresave{$key} = "$nosave";

$key = q/se:FSMTLUDTRNN/;
$ref_files{$key} = "$dir".q|node48.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_lewis81b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:TMDTRNN/;
$ref_files{$key} = "$dir".q|node66.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_baltersee97p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/ch:GIDTRNN/;
$ref_files{$key} = "$dir".q|node69.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_haykin98b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_cauwenberghs93p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Hor89a/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chovan94p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/fg:NARX/;
$ref_files{$key} = "$dir".q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_lin96j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:TLU/;
$ref_files{$key} = "$dir".q|node11.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:TDNN/;
$ref_files{$key} = "$dir".q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_mcculloch60b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:elmanhi/;
$ref_files{$key} = "$dir".q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/ch:FANNI/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_rashevsky38b/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_lawrence96p/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/se:SP/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_lang90j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:Alopex/;
$ref_files{$key} = "$dir".q|node31.html|; 
$noresave{$key} = "$nosave";

$key = q/ss:neuMe/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/se:DTRNNSP/;
$ref_files{$key} = "$dir".q|node25.html|; 
$noresave{$key} = "$nosave";

$key = q/pg:teachfor/;
$ref_files{$key} = "$dir".q|node38.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_siegelmann95j/;
$ref_files{$key} = "$dir".q|node95.html|; 
$noresave{$key} = "$nosave";

$key = q/fg:mealy/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

1;

