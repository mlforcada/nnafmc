%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: (atend)
%%Title: TeXdraw drawing: kleene.ps1
%%Pages: 1
%%Creator: 1995/12/19 TeXdraw V2R0
%%CreationDate: 2002/1/21
50 dict begin
/mv {stroke moveto} def
/lv {lineto} def
/st {currentpoint stroke moveto} def
/sl {st setlinewidth} def
/sd {st 0 setdash} def
/sg {st setgray} def
/bs {gsave} def /es {stroke grestore} def
/fl {gsave setgray fill grestore
 currentpoint newpath moveto} def
/fp {gsave setgray fill grestore st} def
/cv {curveto} def
/cr {gsave currentpoint newpath 3 -1 roll 0 360 arc
 stroke grestore} def
/fc {gsave setgray currentpoint newpath
 3 -1 roll 0 360 arc fill grestore} def
/ar {gsave currentpoint newpath 5 2 roll arc stroke grestore} def
/el {gsave /svm matrix currentmatrix def
 currentpoint translate scale newpath 0 0 1 0 360 arc
 svm setmatrix stroke grestore} def
/fe {gsave setgray currentpoint translate scale newpath
 0 0 1 0 360 arc fill grestore} def
/av {/hhwid exch 2 div def /hlen exch def
 /ah exch def /tipy exch def /tipx exch def
 currentpoint /taily exch def /tailx exch def
 /dx tipx tailx sub def /dy tipy taily sub def
 /alen dx dx mul dy dy mul add sqrt def
 /blen alen hlen sub def
 gsave tailx taily translate dy dx atan rotate
 (V) ah ne {blen 0 gt {blen 0 lineto} if} {alen 0 lineto} ifelse
 stroke blen hhwid neg moveto alen 0 lineto blen hhwid lineto
 (T) ah eq {closepath} if
 (W) ah eq {gsave 1 setgray fill grestore closepath} if
 (F) ah eq {fill} {stroke} ifelse
 grestore tipx tipy moveto} def
0.24 0.24 scale
1 setlinecap 1 setlinejoin
3 setlinewidth [] 0 setdash
0 0 moveto
2 sl
bs
0 65 mv
65 65 lv
32 0 lv
0 65 lv
es
32 65 mv
130 169 lv
130 169 mv
bs
134 174 mv
6 0 fc
es
bs
130 65 mv
195 65 lv
162 0 lv
130 65 lv
es
162 65 mv
234 169 lv
234 169 mv
bs
238 174 mv
6 0 fc
es
bs
260 65 mv
325 65 lv
292 0 lv
260 65 lv
es
292 65 mv
292 169 lv
292 169 mv
bs
292 175 mv
6 0 fc
es
bs
390 65 mv
455 65 lv
422 0 lv
390 65 lv
es
422 65 mv
351 169 lv
351 169 mv
bs
347 174 mv
6 cr
es
bs
520 65 mv
585 65 lv
552 0 lv
520 65 lv
es
552 65 mv
455 169 lv
455 169 mv
bs
451 174 mv
6 cr
es
bs
260 260 mv
325 260 lv
292 195 lv
260 260 lv
es
292 260 mv
292 364 lv
292 364 mv
bs
292 370 mv
6 0 fc
es
stroke end showpage
%%Trailer:
%%BoundingBox: 0 -3 140 90
%%EOF
